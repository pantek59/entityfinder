from tqdm import tqdm
import argparse
import csv
import json
import requests
from requests.exceptions import HTTPError
import sys
import urllib.parse


def lengths(x):
    if isinstance(x,list):
        yield len(x)
        for y in x:
            yield from lengths(y)


api_list = list()
first_entry = True
too_long_entries = list()
max_uri_length = 5000

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file",
                    help="CSV formatted file containing the text to be analyzed (one column without headers, one entry per row)")
parser.add_argument("-k", "--key", help="RapidAPI Key to use with RapidAPIs")
parser.add_argument("-mc", "--meaningcloud",
                    help="Perform Meaning Cloud Topic Extraction (https://rapidapi.com/MeaningCloud/api/topics-extraction)",
                    action="store_true")
parser.add_argument("-qs", "--quasiris",
                    help="Perform Quasiris Topic Extraction (https://rapidapi.com/quasiris/api/entity-extraction)",
                    action="store_true")
parser.add_argument("-tc", "--taconcepts",
                    help="Perform Text Analysis Concept Extraction (https://rapidapi.com/aylien/api/text-analysis)",
                    action="store_true")
parser.add_argument("-th", "--tahashtags",
                    help="Perform Text Analysis Hash-Tagging (https://rapidapi.com/aylien/api/text-analysis)",
                    action="store_true")
parser.add_argument("-te", "--taentities",
                    help="Perform Text Analysis Entity Extraction (https://rapidapi.com/aylien/api/text-analysis)",
                    action="store_true")
parser.add_argument("-tw", "--twinword",
                    help="Perform Twin Word Keywords & Topics Extraction (https://rapidapi.com/twinword/api/topic-tagging)",
                    action="store_true")
args = parser.parse_args()

if not args.file:
    print("Please indicate CSV file containing text bits to analyze. Use \"entityFinder -h\" for help.")
    sys.exit()
if not args.key:
    try:
        keyfile = open("key.cfg", "r")
    except (IOError, OSError):
        print("Use option '-k' to provide RapidAPI key or put the key in a text-file named 'key.cfg'.")
        sys.exit()
    else:
        args.key = keyfile.read()
        keyfile.close()

arglist = vars(args)
for arg in arglist:
    if type(arglist[arg]) == bool and arglist[arg] == True:
        api_list.append(arg)

with open(args.file) as f:
    for i, l in enumerate(f):
        pass
    line_max = i + 1
    total_tasks = line_max * len(api_list)



with tqdm(total=total_tasks) as pbar:
    with open(args.file, newline='') as csv_file:
        filecounter = 0

        while True:
            savename = "result-" + str(filecounter) + ".csv"
            try:
                test_handler = open(savename, "r")
            except (OSError, IOError):
                break
            else:
                # Increase counter for result file
                filecounter += 1
                test_handler.close()
        result_handler = open(savename, "w")
        write_handler = csv.writer(result_handler, delimiter=';')

        result_headers = list()
        result_headers.append("#")
        result_headers.append("Input Text")
        read_handler = csv.reader(csv_file, delimiter=';')
        row_counter = 0

        for row in read_handler:
            result_columns = list()
            row_counter += 1
            text = row[0]
            if len(text) > max_uri_length:
                long_entry = {"row": row_counter, "new_length": max_uri_length, "old_length": len(text)}
                text = text[:max_uri_length]
                too_long_entries.append(long_entry)

            for api in api_list:
                if api == "meaningcloud":
                    pbar.set_description("Meaning Cloud Topic Extraction running ...")
                    result_headers.append("Meaning Cloud Entity List")
                    result_headers.append("Meaning Cloud Concept List")
                    result_headers.append("Meaning Cloud Time Expression List")
                    result_headers.append("Meaning Cloud Money Expression List")
                    result_headers.append("Meaning Cloud Quantity Expression List")
                    result_headers.append("Meaning Cloud Other Expression List")
                    result_headers.append("Meaning Cloud Quotation List")
                    result_headers.append("Meaning Cloud Relation List")
                    textstring = text.replace(" ", "+").replace(u"\u2018", "'").replace(u"\u2019", "'").replace(
                            u"\u2014", "-").replace(u"\u201c", '"').replace(u"\u201d", "'")
                    try:
                        response = requests.get(
                            "https://topics-extraction.p.rapidapi.com/topics-2.0.php?txtf=plain&uw=n&rt=n&dm=s&sdg=l&st=n&of=json&lang=en&txt=" + textstring + "&tt=a",
                            headers={
                                "X-RapidAPI-Host": "topics-extraction.p.rapidapi.com",
                                "X-RapidAPI-Key": args.key,
                                "accept": "application/json;"
                            },
                            )
                    except HTTPError as http_err:
                        print(f'HTTP error occurred: {http_err}')
                    except Exception as err:
                        print(f'Other error occurred: {err}')
                    else:
                        if response.status_code == 200:
                            rep = json.loads(response.text)
                            lists = list()
                            lists.append("entity_list")
                            lists.append("concept_list")
                            lists.append("time_expression_list")
                            lists.append("money_expression_list")
                            lists.append("quantity_expression_list")
                            lists.append("other_expression_list")
                            lists.append("quotation_list")
                            lists.append("relation_list")

                            for listname in lists:
                                temp_column = list()
                                for entry in rep[listname]:
                                    temp_column.append(entry["form"])
                                result_columns.append(temp_column)

                        else:
                            error_column = list();
                            error_column.append("Error: " + str(response.status_code))
                            for i in range(7):
                                result_columns.append(error_column)

                    pbar.update(1)
                elif api == "quasiris":
                    pbar.set_description("Quasiris Topic Extraction running ...")

                    result_headers.append("Quasiris Intention")
                    result_headers.append("Quasiris Intention Score")
                    result_headers.append("Quasiris Classification")
                    result_headers.append("Quasiris Classification Score")
                    result_headers.append("Quasiris Scope")
                    result_headers.append("Quasiris Scope Score")

                    textstring = urllib.parse.quote(text)
                    try:
                        response = requests.get(
                            "https://entities.p.rapidapi.com/?query=" + textstring,
                            headers={
                                "X-RapidAPI-Host": "entities.p.rapidapi.com",
                                "X-RapidAPI-Key": args.key,
                            },
                        )
                    except HTTPError as http_err:
                        print(f'HTTP error occurred: {http_err}')
                    except Exception as err:
                        print(f'Other error occurred: {err}')
                    else:
                        if response.status_code == 200:
                            rep = json.loads(response.text)
                            lists = list()
                            lists.append("searchIntention")
                            lists.append("searchClassification")
                            lists.append("searchScope")

                            for listname in lists:
                                if len(rep[listname]["topics"]) > 0:
                                    topic_column = list()
                                    score_column = list()
                                    for topic in rep[listname]["topics"]:
                                        topic_column.append(topic["name"])
                                        score_column.append(topic["score"])
                                    result_columns.append(topic_column)
                                    result_columns.append(score_column)
                        else:
                            error_column = list()
                            error_column.append("Error: " + str(response.status_code))
                            for i in range(6):
                                result_columns.append(error_column)

                    pbar.update(1)
                elif api == "taconcepts":
                    pbar.set_description("Text Analysis Concepts Extraction running ...")
                    result_headers.append("Text Analysis Concepts")
                    textstring = urllib.parse.quote(text)
                    try:
                        response = requests.get(
                            "https://aylien-text.p.rapidapi.com/concepts?text=" + textstring,
                            headers={
                                "X-RapidAPI-Host": "aylien-text.p.rapidapi.com",
                                "X-RapidAPI-Key": args.key,
                            },
                        )
                    except HTTPError as http_err:
                        print(f'HTTP error occurred: {http_err}')
                    except Exception as err:
                        print(f'Other error occurred: {err}')
                    else:
                        if response.status_code == 200:
                            rep = json.loads(response.text)
                            entries = list(rep["concepts"].keys())
                            result_columns.append(entries)
                        else:
                            error_column = list();
                            error_column.append("Error: " + str(response.status_code))
                            result_columns.append(error_column)
                    pbar.update(1)
                elif api == "tahashtags":
                    pbar.set_description("Text Analysis Hashtags running ...")
                    result_headers.append("Text Analysis Hashtags")
                    textstring = urllib.parse.quote(text)
                    try:
                        response = requests.get(
                            "https://aylien-text.p.rapidapi.com/hashtags?text=" + textstring,
                            headers={
                                "X-RapidAPI-Host": "aylien-text.p.rapidapi.com",
                                "X-RapidAPI-Key": args.key,
                            },
                        )
                    except HTTPError as http_err:
                        print(f'HTTP error occurred: {http_err}')
                    except Exception as err:
                        print(f'Other error occurred: {err}')
                    else:
                        if response.status_code == 200:
                            rep = json.loads(response.text)
                            temp_column = list()
                            for tag in rep["hashtags"]:
                                temp_column.append(tag)
                            result_columns.append(temp_column)
                        else:
                            error_column = list();
                            error_column.append("Error: " + str(response.status_code))
                            result_columns.append(error_column)
                    pbar.update(1)
                elif api == "taentities":
                    pbar.set_description("Text Analysis Entity Extraction running ...")
                    result_headers.append("Text Analysis Keywords")
                    result_headers.append("Text Analysis Dates")
                    result_headers.append("Text Analysis Locations")
                    result_headers.append("Text Analysis Persons")

                    textstring = urllib.parse.quote(text)
                    try:
                        response = requests.get(
                            "https://aylien-text.p.rapidapi.com/entities?text=" + textstring,
                            headers={
                                "X-RapidAPI-Host": "aylien-text.p.rapidapi.com",
                                "X-RapidAPI-Key": args.key,
                            },
                        )
                    except HTTPError as http_err:
                        print(f'HTTP error occurred: {http_err}')
                    except Exception as err:
                        print(f'Other error occurred: {err}')
                    else:
                        if response.status_code == 200:
                            rep = json.loads(response.text)
                            keywords = list()
                            data = list()
                            locations = list()
                            persons = list()
                            if "keyword" in rep["entities"]:
                                keywords = list(rep["entities"]["keyword"])
                            if "date" in rep["entities"]:
                                dates = list(rep["entities"]["date"])
                            if "location" in rep["entities"]:
                                locations = list(rep["entities"]["location"])
                            if "person" in rep["entities"]:
                                persons = list(rep["entities"]["person"])

                            result_columns.append(keywords)
                            result_columns.append(dates)
                            result_columns.append(locations)
                            result_columns.append(persons)
                        else:
                            error_column = list()
                            error_column.append("Error: " + str(response.status_code))
                            for i in range(4):
                                result_columns.append(error_column)
                    pbar.update(1)
                elif api == "twinword":
                    pbar.set_description("Twin Word Keywords & Topics Extraction running ...")
                    result_headers.append("Twinword Keywords")
                    result_headers.append("Twinword Topics")
                    textstring = urllib.parse.quote(text)
                    try:
                        response = requests.get(
                            "https://twinword-topic-tagging.p.rapidapi.com/generate/?text=" + textstring,
                            headers={
                                "X-RapidAPI-Host": "twinword-topic-tagging.p.rapidapi.com",
                                "X-RapidAPI-Key": args.key,
                            },
                        )
                    except HTTPError as http_err:
                        print(f'HTTP error occurred: {http_err}')
                    except Exception as err:
                        print(f'Other error occurred: {err}')
                    else:
                        if response.status_code == 200:
                            rep = json.loads(response.text)
                            keywords = list(rep["keyword"].keys())
                            topics = list(rep["topic"].keys())
                            result_columns.append(keywords)
                            result_columns.append(topics)
                        else:
                            error_column = list()
                            error_column.append("Error: " + str(response.status_code))
                            for i in range(2):
                                result_columns.append(error_column)
                    pbar.update(1)

            # Entry Output
            if first_entry:
                write_handler.writerow(result_headers)
                first_entry = False
            max_len = max(lengths(result_columns))
            for i in range(max_len):
                newrow = list()
                newrow.append(row_counter)
                newrow.append(text)
                for column in result_columns:
                    if len(column) > i:
                        newrow.append(column[i])
                    else:
                        newrow.append("-")
                write_handler.writerow(newrow)
print("Done.")
if len(too_long_entries) > 0:
    for e in too_long_entries:
        print(f"Row {e['row']} had {e['old_length']} characters and has been shortened to {e['new_length']}.")
print("Result file: " + savename)
